Babel<3.0,>=1.0
django-extra-views<0.14,>=0.13
django-haystack>=3.0b1
django-phonenumber-field<4.0.0,>=3.0.0
django-tables2<2.3,>=2.2
django-treebeard>=4.3.0
django-widget-tweaks>=1.4.1
django<3.1,>=2.2
factory-boy<3.0,>=2.4.1
phonenumbers
pillow>=6.0
purl>=0.7

[docs]
Sphinx==2.2.2
easy-thumbnails<2.8,>=2.7
sorl-thumbnail<12.7,>=12.6
sphinx-issues==1.2.0
sphinx_rtd_theme==0.4.3
sphinxcontrib-napoleon==0.7
sphinxcontrib-spelling==4.3.0

[easy-thumbnails]
easy-thumbnails<2.8,>=2.7

[sorl-thumbnail]
sorl-thumbnail<12.7,>=12.6

[test]
WebTest<2.1,>=2.0
coverage<5.1,>=5.0
django-webtest<1.10,>=1.9
easy-thumbnails<2.8,>=2.7
freezegun<0.4,>=0.3
psycopg2-binary<2.9,>=2.8
pytest-django<3.8,>=3.7
pytest-xdist<1.32,>=1.31
sorl-thumbnail<12.7,>=12.6
tox<3.15,>=3.14
